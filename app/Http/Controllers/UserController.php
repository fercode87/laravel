<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller {

    public function pruebas(request $request) {
        return 'pruebas de usuario';
    }

    public function register(request $request) {

        //recoger datos de usuario
        $json = $request->input('json', null);
        $params = json_decode($json); //datos volcados a un objeto
        $paramsarray = json_decode($json, true); //datos volcados a un array
//limpiar espacios de adelante y atras de los datos 
        if (!empty($params) && !empty($paramsarray)) {
            $paramsarray = array_map('trim', $paramsarray);

//validar datos
            $validate = \Validator::make($paramsarray, [
                        'name' => 'required|alpha',
                        'surname' => 'required|alpha',
                        'email' => 'required|email|unique:users', //comprobar si esta duplicado el usuario
                        'password' => 'required'
            ]);

//si la validacion falla
            if ($validate->fails()) {
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'el usuario no se genero intentelo nuevamente',
                    'errors' => $validate->errors(),
                );

//si la validacion es positiva 
            } else {
//cifrar contraseña
                $pwd = hash('sha256', $params->password);


                //crear el usuario
                $user = new User();
                $user->name = $paramsarray ['name'];
                $user->role = 'ROLE_USER';
                $user->surname = $paramsarray ['surname'];
                $user->email = $paramsarray ['email'];
                $user->password = $pwd;
                //guarda usuario
                $user->save();
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'el usuario se genero correctamente',
                    'User' => $user
                );
            }
            //si los datos estan mal ingresados
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'los datos ingresados no son validos',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function login(request $request) {

        $jwtAuth = new \JwtAuth();
        //recibir datos por post
        $json = $request->input('json', null);
        $params= json_decode($json);
        $params_array =json_decode($json, true);
        //vlidar datos
        $validate = \Validator::make($params_array, [
                    'email' => 'required|email',
                    'password' => 'required'
        ]);

//si la validacion falla
        if ($validate->fails()) {
            $signup = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'el usuario no se pudo identificar',
                'errors' => $validate->errors(),
            );

//si la validacion es positiva 
        } else {  
            $pwd = hash('sha256', $params->password);
            $signup = $jwtAuth->signup($params->email,$pwd);
        if(!empty($params->gettoken)){
            $signup = $jwtAuth->signup($params->email,$pwd, true);
        }
            
        }
       return response()->json($signup, 200);
    }
    public function update(request $request) {
        $token = $request->header('Authorization');
        $jwtAuth = new \JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
        //tomar datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        $user = $jwtAuth->checkToken($token, true);
        if ($checkToken && !empty($params_array)) {
    //validar
            $validate = \Validator::make($params_array, [
               'name' => 'required|alpha',
               'surname' => 'required|alpha',
               'email' => 'required|email|unique:users' . $user->sub
            ]);
    //datos q no quiero actualizar
            unset($params_array['id']);
            unset($params_array['password']);
            unset($params_array['role']);
            unset($params_array['created_at']);
            unset($params_array['remember_token']);
            $user_update = user::where('id', $user->sub)->update($params_array);
            //devolver array con resultado de la peticion
            $data = array(
                'status' => 'success',
                'code' => 200,
                'user'=> $user,
                'changes'=>$params_array,
                'message' => 'los datos se actualizaron correctamente',
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'los datos ingresados no son validos',
            );
        }
        return response()->json($data, $data['code']) ;
    }
    public function upload(request $request) {
        //recoger los datos de la peticion de imagen
        $image = $request->file('file0');
        //validar imagen        
        $validate = \Validator::make($request->all(), [
                    'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);
        if (!$image || $validate->fails()) {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'la imagen no se subio correctamente'
            );
        } else {//guardar imagen

            $image_name = time() . $image->getClientOriginalName();
            \Storage::disk('Users')->put($image_name, \File::get($image));
            $data = array(
                'status' => 'success',
                'code' => 200,
                'image' => $image_name
            );
        }return Response()->json($data, $data['code']); //->header('content-type', 'text/plain');
    }
    public function getImage($filename){
        $isset=\Storage::disk('Users')->exists($filename);
        if($isset){
            $file=\Storage::disk('Users')->get($filename);
            return new response($file,200);
        }else{$data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'la imagen no existe'
            );
        }return response()->json($data, $data['code']);
            
        
    }

    public function detail($id){
        $user= User::find($id);
        if(is_object($user)){
            $data = array(
                'status' => 'success',
                'code' => 200,
                'user' => $user
            );
        }else{
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'el usuario no existe'
            );
        }
        return response()->json($data, $data['code']);
    }
}
