<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\post;
use Illuminate\Http\Response;
use App\Helpers\JwtAuth;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('api.auth', ['except' =>
            ['index',
                'show',
                'getImage',
                'getPostsByCategory',
                'getPostsByUser']]);
    }

    public function index(){
          $posts= Post::all()->load('category');
                            
          return response()->json([
              'code'=>200,
              'status'=>'success',
              'posts'=>$posts
              
          ]);
      }
      public function show($id) {
        $post = Post::find($id)->load('category')
                                ->load('user');
        if (is_object($post)) {

            $data = [
                'status' => 'success',
                'code' => 200,
                'post' => $post
            ];
        } else {
            $data = [
                'status' => 'error',
                'code' => 404,
                'message' => 'la entrada no existe'
            ];
        }
        return response()->json($data, $data['code']);
      
      }
      public function store(Request $request) {
        //recoger datos por post
        $json = $request->input('json', null);
        $params = json_decode($json);
        $params_array = json_decode($json, true);

        //validar los datos
        if(!empty($params_array)){
              $user = $this->getIdentity($request);
            
            
        $validate = \Validator::make($params_array, [
                        'title' => 'required',
                        'content' => 'required',
                        'category_id' => 'required',
                        'image' => 'required'
            ]);
            if ($validate->fails()) {
            $data = [
            'code' => 404,
            'message' => 'faltan datos',
            'status' => 'error'
            ];
        } else {// //guardar la categoria
            $post = new Post();
                $post->user_id = $user->sub;
                $post->category_id = $params->category_id;
                $post->title = $params->title;
                $post->content = $params->content;
                $post->image = $params->image;
                $post->save();
                $data = [
                    'code' => 200,
                    'message' => 'la entrada se creo correctamente',
                    'status' => 'success',
                    'post' => $post
                ];
            }
       

//devolver el resultado
    }else{$data = [
            'code' => 404,
           'message' => 'la entrada no se creo correctamente',
            'status' => 'error'
            ];
    }
           return response()->json($data, $data['code']);
      }
      public function update($id, request $request) {
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        $data = [
                    'code' => 200,
                    'message' => 'la entrada se modifico correctamente',
                    'status' => 'success',
                    'post' => $params_array];

        if(!empty($params_array)){
        $validate = \Validator::make($params_array, [
                    'title' => 'required',
                    'content' => 'required',
                    'category_id' => 'required'
        ]);
        if($validate->fails()){
            $data['errors']=$validate->errors();
            return response()->json($data, $data['code']);
        }
        
        unset($params_array['id']);
        unset($params_array['user_id']);
        unset($params_array['created_at']);
        unset($params_array['user']);
        //conseguir usuario identificado
               $user = $this->getIdentity($request);
        //Revisar aca abajo si se puede poner updateOrCreate
        $post= Post::where('id' ,$id)
                   ->where('user_ID', $user->sub)
                   ->first();
        if(!empty($post)&& is_object($post)){
            $post->update($params_array);
        
            $data = [
                'code' => 200,
                'message' => 'la entrada se modifico correctamente',
                'status' => 'success',
                'changes' => $params_array,
                'post' => $post
            ];
        }
        
        }
        return response()->json($data, $data['code']);
    }
    public function destroy($id , request $request){
       $user = $this->getIdentity($request);
        
       $post= Post::where('id' ,$id)
                   ->where('user_ID', $user->sub)
                   ->first();
                   
        
       
       if(!empty($post)){
        $post->delete();
         $data = [
                'code' => 200,
                'message' => 'la entrada se elimino correctamente',
                'status' => 'success',
                'post' => $post
            ];
        }else{$data = [
                'code' => 404,
                'message' => 'la entrada no existe',
                'status' => 'success',
                'post' => $post];
            
        }
        return response()->json($data, $data['code']);
        
    }
    private function getIdentity($request){
        $jwtAuth =new JwtAuth();
        $token=$request->header('Authorization', null);
        $user = $jwtAuth->checkToken($token, true);
        
        return $user;
    }
public function upload(request $request){
    //recoger los datos de la peticion de imagen
        $image = $request->file('file0');
        //validar imagen        
        $validate = \Validator::make($request->all(), [
                    'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);
        if (!$image || $validate->fails()) {
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'la imagen no se subio correctamente'
            );
        } else {//guardar imagen

            $image_name = time() . $image->getClientOriginalName();
            \Storage::disk('Images')->put($image_name, \File::get($image));
            $data = array( 
                'status' => 'success',
                'code' => 200,
                'image' => $image_name
            );
        }return Response()->json($data, $data['code']); //->header('content-type', 'text/plain');
}
public function getImage($filename){
        $isset=\Storage::disk('Images')->exists($filename);
        if($isset){
            $file=\Storage::disk('Images')->get($filename);
            return new response($file,200);
        }else{$data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'la imagen no existe'
            );
        }return response()->json($data, $data['code']);
      
}
public function getPostsByCategory($id){
    $posts=post::where('category_id', $id)->get();
    return response()->json([
        'status'=>'success',
        'posts'=>$posts
        ],200);
    
}
public function getPostsByUser($id){
    $posts=post::where('user_id', $id)->get();
    return response()->json([
        'status'=>'success',
        'posts'=>$posts
        ],200);
    
}
}


