<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use Illuminate\Http\Response;


class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware('api.auth',['except'=>['index','show']]);
    }
    //listartodas las categorias
      public function index(){
          $categories = Category::all();
          
          return response()->json([
              'status'=>'success',
              'code'=>200,
              'categories'=>$categories
              
          ]);
      }
      //mostrar una categoria especifica
      public function show($id) {
        $category = category::find($id);
        if (is_object($category)) {

            $data = [
                'status' => 'success',
                'code' => 200,
                'category' => $category
            ];
        } else {
            $data = [
                'status' => 'error',
                'code' => 404,
                'message' => 'la categoria no existe'
            ];
        }
        return response()->json($data, $data['code']);
    }
    public function store(Request $request) {
        //recoger datos por post
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);

        //validar los datos
        if(!empty($params_array)){
        $validate = \Validator::make($params_array, [
                    'name' => 'required'
        ]);
        if ($validate->fails()) {
            $data = [
            'code' => 400,
            'message' => 'la categoria no se creo correctamente',
            'status' => 'error'
            ];
        } else {// //guardar la categoria
            $category = new Category();
            $category->name = $params_array['name'];
            $category->Save();
            $data = [
            'code' => 200,
            'message' => 'la categoria se creo correctamente',
            'status' => 'success',
            'category'=>$category
            ];
        }
        return response()->json($data, $data['code']);

//devolver el resultado
    }else{$data = [
            'code' => 404,
            'message' => 'no has enviado ninguna categoria.',
            'status' => 'error'
            ];
        
    }

}
public function update($id, request $request) {
        $json = $request->input('json', null);
        $params_array = json_decode($json, true);
        if (!empty($params_array)) {
            $validate = \Validator::make($params_array, [
                        'name' => 'required'
            ]);
            unset($params_array['id']);
            unset($params_array['created_at']);

            $category = Category::where('id', $id)->update($params_array);
            $data = [
                'code' => 200,
                'message' => 'la categoria se modifico correctamente',
                'status' => 'success',
                'category' => $params_array
            ];
        } else {// //guardar la categoria
            $data = [
                'code' => 404,
                'message' => 'la categoria no se modifico correctamente',
                'status' => 'error'
            ];
        }
        return response()->json($data, $data['code']);
    }

}
