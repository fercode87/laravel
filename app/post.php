<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\category;
use App\post;


class post extends Model
{protected $table = 'posts';
   //relacion de uno a muchos inversa
//relacion de uno a muchos, osea traigo el usuario o la categoria
protected $fillable = [
        'title','content','category_id', 'image'
   
    ];

public function user() {
    return $this->belongsTo('App\User', 'user_id');
}
public function category(){
    return $this->belongsTo('App\category', 'category_id');
}}