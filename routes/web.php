<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//definir clases
use \App\Http\Middleware\ApiAuthMiddleware;

Route::get('/', function() {
    return '<h2>Hola Mundo</h2>';
});


Route::get('/welcome', function() {
    return view('welcome');
});   

    
Route::get('/pruebas', function(){
    return '<h2>esto es un titulo</h2>';
});

Route::get('/pruebasCon', 'pruebasController@index');

Route::get('/testorm', 'pruebasController@testorm');


//rutas de pruebas de la API
//Route::get('/entrada/pruebas', 'PostController@Pruebas');
//Route::get('/categoria/pruebas', 'CategoryController@Pruebas');
//Route::get('/usuario/pruebas', 'UserController@Pruebas');
//rutas del controlador de usuarios

Route::Post('/api/register', 'UserController@register');
Route::Post('/api/login', 'UserController@login');
Route::Put('/api/user/update', 'UserController@update');
Route::Post('/api/user/upload', 'UserController@upload')->middleware(ApiAuthMiddleware::class);
Route::get('/api/user/avatar/{filename}', 'UserController@getImage');
Route::get('/api/user/detail/{id}', 'UserController@detail');

//rutas del controlador de categorias
Route::resource('api/category','CategoryController');

    
//rutas del controlador post o entradas
Route::resource('api/post','PostController');
Route::Post('api/post/upload','PostController@upload');
Route::get('/api/post/image/{filename}', 'postController@getImage');
Route::get('/api/post/user/{id}', 'postController@getPostsByUser');
Route::get('/api/post/category/{id}', 'postController@getPostsByCategory');
